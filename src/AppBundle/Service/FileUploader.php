<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * @param StorageFolder
     */
    private $storageFolder;

    public function __construct(ContainerInterface $container)
    {   
        $this->storageFolder = $container->getParameter("storage_path");
    }

    public function upload(UploadedFile $file) : string
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        try {
            $file->move($this->getStorageFolder(), $fileName);
        } catch (FileException $e) {
            throw new FileException("Error: " . $e->getMessage());
        }

        return $fileName;
    }

    public function delete(string $fileName) : bool
    {
        if (\file_exists($this->getStorageFolder() .'/'. $fileName)) {
         return unlink($this->getStorageFolder() .'/'. $fileName);
        }
        return false;
    }

    public function getStorageFolder() : string  
    {
        return $this->storageFolder;
    }
}