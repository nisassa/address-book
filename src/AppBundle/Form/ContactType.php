<?php 

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use Symfony\Component\{
    OptionsResolver\OptionsResolver,
    Validator\Constraints\File,
};
use Symfony\Component\Form\{
    Extension\Core\Type\SubmitType,
    AbstractType,
    Extension\Core\Type\FileType,
    FormBuilderInterface,
    Extension\Core\Type\DateType,
};

class ContactType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('firstName')
            ->add('lastName')
            ->add('streetAndNumber')
            ->add('zip')
            ->add('city')
            ->add('country')
            ->add('phoneNumber')
            ->add('birthday', DateType::class, array(
                'widget' => 'single_text',                
            ))
            ->add('emailAddress')
            ->add('picture')
            ->add('save', SubmitType::class, []);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}