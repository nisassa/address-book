<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ContactType;
use AppBundle\Entity\Contact;
use AppBundle\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;

class AddressBookController extends Controller
{   
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * Contructor
     */
    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    /**
     * @Route("/", name="app_overview")
     */
    public function indexAction(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();

        $contacts = $em->getRepository('AppBundle:Contact')->findAll();
        
        return $this->render('address_book/overview.html.twig', [
            'active_tab' => 'overview',
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/add-edit-new-address/{contact}", name="app_add_new_address")
     */
    public function addNewAddress(Request $request, Contact $contact = null)
    {   
        $filePath = null;
        if (! $contact) {
            $contact = new Contact();
        } else if ($contact->getPicture()) {
            $this->fileUploader->delete($contact->getPicture()); 
            $contact->setPicture(null);
        }

        $form = $this->createForm(ContactType::class, $contact);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $contact = $form->getData();
            if ($contact->getPicture() !== null) {
                $fileName = $this->fileUploader->upload($contact->getPicture());
                $contact->setPicture($fileName);
            }
            
            $em = $this->getDoctrine()->getManager();

            $em->persist($contact);
            $em->flush();
                
            return $this->redirectToRoute('app_overview');
        }        

        return $this->render('address_book/add_new_address.html.twig', [
            'active_tab' => 'add_new_address',
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/delete/{contact}", name="app_contact_delete")
     */
    public function deleteAction(Request $request, Contact $contact)
    {   
        $em = $this->getDoctrine()->getManager();

        if ($contact->getPicture()) {
            $this->fileUploader->delete($contact->getPicture());
        }

        $em->remove($contact);
        $em->flush();
        
        return $this->redirectToRoute('app_overview');
    }

}
